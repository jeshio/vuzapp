package com.jeshio.vuzup;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jeshio.vuzup.helpers.TimetableDbHelper;

import org.json.JSONObject;

/**
 * Created by jeshio on 22.12.14.
 */
public class PairsItem {

    public String name;
    public String date;
    public String auditory;
    public String subgroup;
    public String begin;
    public String end;
    public String group;
    public String teacher;

    PairsItem(JSONObject obj, String date) {
        name = getElement(obj, "name");
        auditory = getElement(obj, "auditory");
        subgroup = getElement(obj, "subgroup");
        begin = getElement(obj, "begin");
        end = getElement(obj, "end");
        teacher = getElement(obj, "teacher");
        group = getElement(obj, "group");
        this.date = date;
    }

    PairsItem(Cursor cursor) {
        date = cursor.getString(2);
        begin = cursor.getString(3);
        end = cursor.getString(4);
        group = cursor.getString(5);
        subgroup = cursor.getString(6);
        name = cursor.getString(7);
        teacher = cursor.getString(8);
        auditory = cursor.getString(9);
    }

    @Override
    public String toString() {
        return "name: " + name +
                ", date: " + date;
    }

    public void putToDb(SQLiteDatabase db) {
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_AUDITORY, this.auditory);
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_BEGIN, this.begin);
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_END, this.end);
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_DATE, this.date);
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_NAME, this.name);
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_GROUP, this.group);
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_SUBGROUP, this.subgroup);
        values.put(TimetableDbHelper.TimetableEntry.COLUMN_NAME_TEACHER, this.teacher);

// Insert the new row, returning the primary key value of the new row
        db.insert(
                TimetableDbHelper.TimetableEntry.TABLE_NAME,
                null,
                values);
    }

    private String getElement(JSONObject o, String col) {
        try {
            return o.getString(col);
        } catch (Exception e) {
            return "";
        }
    }
}