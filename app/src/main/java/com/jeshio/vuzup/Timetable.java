package com.jeshio.vuzup;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.jeshio.vuzup.helpers.TimetableDbHelper;
import com.jeshio.vuzup.helpers.Transliterator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ru.vuzapp.app.R;

public class Timetable extends Fragment {
    protected ArrayList<WeekDayItem> week_days = new ArrayList<WeekDayItem>();
    private String timetable_server;
    private View rootView;
    private WeekDayItemAdapter weekDaysArrayAdapter;
    private ListView week_day_list;

    private String prepod_api = "http://api.vuzapp.ru/1.0/teachers.timetable.get?api_key=cxUKa1PhMjXALzxTYFK27BGniN5YnHSi&key=ugrasu&teacher=";
    private String student_api = "http://api.vuzapp.ru/1.0/students.timetable.get?api_key=cxUKa1PhMjXALzxTYFK27BGniN5YnHSi&key=ugrasu&group=";

    private String date_format = "yyyy-MM-dd";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.timetable, container, false);

        Bundle bundle = this.getArguments();
        String oneShot = new String();
        if (bundle != null) {
            oneShot = bundle.getString(MyApp.oneShot, "");
        }

        week_day_list = (ListView) rootView.findViewById(R.id.weekDaysList);

        Button btn = (Button) rootView.findViewById(R.id.update_timetable);

        if (Perferences.studentApi(rootView.getContext()))
            timetable_server = student_api;
        else
            timetable_server = prepod_api;

        weekDaysArrayAdapter = new WeekDayItemAdapter(rootView.getContext(), week_days);

        week_day_list.setAdapter(weekDaysArrayAdapter);

        if (oneShot.isEmpty()) {
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    timetable_to_db();
                }
            });

            get_timetable();
        } else {
            String param;
            if (oneShot.matches(".*[0-9]+.*")) // группа
                param = student_api;
            else
                param = prepod_api;

            String search = param + Transliterator.transliterate(
                    oneShot.replace(".", "").replace(" ", "_"));


            new OneTimetableTask().execute(search);
        }

        return rootView;
    }

    public void get_timetable() {
        String last_update = Perferences.getString(getActivity(), Perferences.LAST_UPDATE, "");

        SimpleDateFormat dateParser = new SimpleDateFormat("dd-MM-yyyy");

        try {
            java.util.Date last_update_date = dateParser.parse(last_update);

            // если расписание получено на этой неделе
            Calendar cal = Calendar.getInstance();
            int current_week = cal.get(Calendar.WEEK_OF_YEAR);

            cal.setTime(last_update_date);
            int update_week = cal.get(Calendar.WEEK_OF_YEAR);

            if (current_week == update_week) {
                timetable_from_db();
            } else {
                timetable_to_db();
            }

        } catch (Exception e) {
            timetable_to_db();
        }

    }

    // устанавливает расписание из БД
    public void timetable_from_db() {
        TimetableDbHelper dbHelper = new TimetableDbHelper(getActivity());
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // 1. build the query
        String query = "SELECT * FROM " + TimetableDbHelper.TimetableEntry.TABLE_NAME;

        // 2. get reference to writable DB
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        if (cursor.moveToFirst()) {
            week_days.clear();
            Map<String, ArrayList<PairsItem>> pairs = new HashMap<String, ArrayList<PairsItem>>();
            //ArrayList<PairsItem> pairs;// = new ArrayList<PairsItem>();

            do {
                PairsItem pair = new PairsItem(cursor);

                if (pairs.get(pair.date) == null) {
                    pairs.put(pair.date, new ArrayList<PairsItem>());
                }
                pairs.get(pair.date).add(pair);
            } while (cursor.moveToNext());


            Calendar current = Calendar.getInstance();
            current.set(current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DATE), 0, 0, 0);
            current.clear(Calendar.MILLISECOND);

            for (Map.Entry<String, ArrayList<PairsItem>> entry :
                    pairs.entrySet()) {
                String key = entry.getKey();
                SimpleDateFormat dateParser = new SimpleDateFormat(date_format);

                try {
                    java.util.Date realDate = dateParser.parse(key);

                    if (realDate.getTime() != current.getTime().getTime() && realDate.getTime() - 24 * 60 * 60 * 1000 !=
                            current.getTime().getTime()) {
                        // continue; // Показывать только сегоднящние и завтрашние
                    }
                } catch (Exception e) {

                }

                week_days.add(new WeekDayItem(key, entry.getValue()));
            }

            weekDaysArrayAdapter.notifyDataSetChanged();
        } else {
            timetable_to_db();
        }

        db.close();
    }

    // записывает данные расписания в БД и выводит на экран
    public void timetable_to_db() {
        new TimetableDBTask().execute(timetable_server);
    }

    class OneTimetableTask extends TimetableTask {
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            for (WeekDayItem wd :
                    week_days) {
                Timetable.this.week_days.add(wd);
            }
            if (week_days.size() == 0 && isAdded())
                Toast.makeText(rootView.getContext(), getResources().getString(R.string.empty_timetable), Toast.LENGTH_SHORT).show();
            weekDaysArrayAdapter.notifyDataSetChanged();
        }
    }

    class TimetableDBTask extends TimetableTask {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            find_query = Transliterator.transliterate(
                    Perferences.getString(rootView.getContext(), Perferences.TIMETABLE_MAIN_DATA, "").
                            replace(".", "").replace(" ", "_"));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            JSONArray arr;

            TimetableDbHelper dbHelper = new TimetableDbHelper(getActivity());

            try {

                JSONObject jObject = new JSONObject(result);

                jObject = jObject.getJSONObject("response");

                arr = jObject.getJSONArray("items");

                // очистить БД
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete(TimetableDbHelper.TimetableEntry.TABLE_NAME, null, null);

                for (int i = 0; i < arr.length(); i++) {

                    JSONObject JWeekDays = arr.getJSONObject(i);

                    // *

                    JSONArray JWeekDay = JWeekDays.getJSONArray("items");

                    // добавление в БД
                    for (int j = 0; j < JWeekDay.length(); j++) {
                        PairsItem pair = new PairsItem(JWeekDay.getJSONObject(j), JWeekDays.getString("date_d").trim());
                        pair.putToDb(db);
                    }
                }

                timetable_from_db();

                SimpleDateFormat dateParser = new SimpleDateFormat("dd-MM-yyyy");
                Perferences.setString(getActivity(), Perferences.LAST_UPDATE, dateParser.format(new Date()));
                db.close();

            } catch (Exception e) {
                if (isAdded())
                    Toast.makeText(rootView.getContext(), getResources().getString(R.string.bad_connect), Toast.LENGTH_SHORT).show();
            }
        }
    }
}