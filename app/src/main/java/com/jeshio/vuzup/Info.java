package com.jeshio.vuzup;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 06.10.14.
 */
public class Info extends Fragment {
    private WebView mWebView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.info, container, false);

        mWebView = (WebView) rootView.findViewById(R.id.webviewInfo);
        // включаем поддержку JavaScript
        mWebView.getSettings().setJavaScriptEnabled(true);
        // указываем страницу загрузки
        String content = "<html>\n" +
                "<head>\n" +
                "<meta charset=\"utf-8\">\n" +
                "<link rel=\"stylesheet\" href=\"file:///android_asset/onsenui.css\">\n" +
                "<link rel=\"stylesheet\" href=\"file:///android_asset/onsen-css-components.min.css\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<ons-tabbar>\n" +
                "<ons-tab page=\"student\" label=\"Студенту\" active=\"true\"></ons-tab>\n" +
                "<ons-tab page=\"abiturient\" label=\"Абитуриенту\" onclick=\"SendRequestAbiturient('http://api.vuzapp.ru/1.0/AndroidApp/abiturientDir.php?key=ugrasu&api_key=cxUKa1PhMjXALzxTYFK27BGniN5YnHSi');\"></ons-tab>\n" +
                "<ons-tab page=\"search\" label=\"Поиск\"></ons-tab>\n" +
                "</ons-tabbar>\n" +
                "<ons-template id=\"student\">\n" +
                "<div style=\"margin-top: 49px;\">\n" +
                "<div class=\"acc-container\">\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Заселение в общежитие</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\" style=\"padding: 0 16px;\">\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Заселение первокурсников будет осуществляться с 28 августа в соответствии со списком, размещенном на сайте университета в разделе официальных документов студенческого городка.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Для заселения в общежитие вам нужно предоставить следующие документы:\n" +
                "<br> - паспорт;\n" +
                "<br> - флюорография;\n" +
                "<br> - 2 фотографии 3x4;\n" +
                "<br> - документы, подтверждающие льготу по оплате стоимости проживания (справка о составе семьи, декларация о доходах всех членов семьи (2 НДФЛ)).\n" +
                "</p>\n" +
                "<br>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Карта</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\" style=\"padding: 0 16px;\">\n" +
                "<center><button onclick='window.open(\"http://ugrasu.ru/upload/medialibrary/dd3/dd3373eadb4a2ec0dd58a595f861801c.jpg\", \"_blank\")' class=\"button\" style=\"padding-bottom: 5px; margin-top: 5px; height: 36px; width: 100%;\">Открыть карту корпусов</button>\n" +
                "<button onclick='window.open(\"https://www.google.com/maps/d/u/0/viewer?mid=zzmcDF61iEo0.kh5MoUw5-CEI\", \"_blank\")' class=\"button\" style=\"padding-bottom: 5px; margin-top: 5px; height: 36px; width: 100%;\">Интерактивная карта</button>\n" +
                "</center>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Интерактивная карта поможет вам сориентироваться в жилом комплексе: узнать, где находится корпус, в котором вам предстоит жить; где остановки общественного транспорта, и какие маршруты к ним привязаны; где ближайшие магазины и спортивные объекты.</p>\n" +
                "<br>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Студенческие сообщества вк</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\">\n" +
                "<div class=\"card-content\">\n" +
                "<div class=\"list-block media-list\">\n" +
                "<ul style=\"-webkit-padding-start: 0px;\">\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/vuzapp\", \"_blank\")'>\n" +
                "<div class=\"item-media\">\n" +
                "<img src=\"file:///android_asset/images/39U3QJKtgS4.jpg\" width=\"44\">\n" +
                "</div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">VuzApp.ru</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Разработчики приложения ЮГУ</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/ssc_sever\", \"_blank\")'>\n" +
                "<div class=\"item-media\">\n" +
                "<img src=\"file:///android_asset/images/ymKvGphSdqQ.jpg\" width=\"44\">\n" +
                "</div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">ССК СЕВЕР</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Спортивный клуб ЮГУ</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/so_ugrasu\", \"_blank\")'>\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/NJN98Ryt_pQ.gif\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Совет обучающихся ЮГУ</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Совет</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/podslushano_ugrasu\", \"_blank\")'>\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/lfmr-uYjQgk.gif\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Подслушано ЮГУ</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Здесь говорят о тебе</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/gor_vozrast\", \"_blank\")'>\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/pTc6pUYgulw.jpg\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Горячий Возраст</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Тележурнал</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/volunteer_ugrasu\", \"_blank\")'>\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/a_e36434a2.gif\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Волонтерский центр ЮГУ</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">vc.ugrasu.ru</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/public86022855\", \"_blank\")'>\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/y_b293270e.gif\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Твои студенческие новости</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Тележурнал</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "</li>\n" +
                "<li class=\"item-content\" onclick='window.open(\"http://vk.com/aso_russia\", \"_blank\")'>\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/UmGFoM4I14I.gif\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">АСО России</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Организация</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Об университете</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\" style=\"padding: 0 16px;\">\n" +
                "<center><img width=\"170px\" height=\"170px\" src=\"file:///android_asset/images/270px-Ugra_state_university_emblem.gif\"></center>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;20 августа 2001 года было подписано распоряжение Правительства России № 1069-р о создании в Ханты-Мансийске государственного образовательного учреждения высшего профессионального образования «Югорский государственный университет». Университет начал работу на базе ранее существовавших учебных заведений: Всероссийской государственной налоговой академии, Нижневартовского государственного педагогического института, Сибирской автомобильно-дорожной академии, Томского государственного университета систем управления и радиоэлектроники и Тюменской государственной сельскохозяйственной академии.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;В настоящее время Югорский государственный университет является одним из самых молодых государственных вузов России.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Особая заслуга по созданию университета принадлежит  Александру Филипенко (Губернатор Ханты-Мансийского автономного округа - Югры 1995-2010 гг.). Под его руководством была проведена планомерная, долгая и кропотливая работа, в результате которой 20 августа 2001 года премьер-министром Российской Федерации было подписано распоряжение о создании в Ханты-Мансийске государственного образовательного учреждения высшего профессионального образования \"Югорский государственный университет\".Важнейшей областью деятельности ЮГУ является подготовка кадров для промышленного сектора экономики.</p>\n" +
                "<br>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>О городе</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\" style=\"padding: 0 16px;\">\n" +
                "<center><img src=\"file:///android_asset/images/KhwmL82WeDE.jpg\" width=\"100%\"></center>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Ханты-Мансийск - один из самых красивых и необычных городов Западной Сибири, обладающий уникальным природным ландшафтом, архитектурным обликом, высоким уровнем сервиса, развитой инфраструктурой. В Ханты-Мансийске создано свое собственное, отличное от большинства российских провинций, городское пространство, которое сформировано благодаря выполнению столичных функций, но также имеет и сложившуюся местную специфику.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Много чего в Ханты-Мансийске «самого-самого». Например, это самый освещенный город - 100% улиц города освещены. Это самый здоровый город – здесь 30% населения регулярно занимается спортом, а среди молодежи – больше 60%.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;На совсем небольшой по площади территории городского пространства сконцентрированы 165 спортивные сооружений, включая объекты самого серьезного уровня: ледовая арена «Югра» вместимостью 6 тысяч зрителей и рядом еще один стадион поменьше, открытый стадион с трибунами на 5 тысяч зрителей, современный теннисный корт, несколько спортивных комплексов, оснащенных современным оборудованием.</p>\n" +
                "<img src=\"file:///android_asset/images/rEmikDJKZMg.jpg\" width=\"100%\">\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Туристические ориентиры Ханты-Мансийска, осознанно взятые в свое время окружной властью, и стали причиной столь требовательного отношения к организации городского пространства. На улицах и площадях Ханты-Мансийска установлено порядка 60 памятников и скульптурных композиций. Два парка с березовыми рощами - парк Бориса Лосева и парк Победы – настоящие украшения города. В парках есть скамейки и фонтаны, тропинки и беседки, ротонды и мостики.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Географически Ханты-Мансийск – очень компактный город, поэтому трудно точно сказать, где кончается центр и начинается периферия. Наверное, как таковой ее ещё нет. Это очень гармоничный, «собранный с боков» город. Все районы Ханты-Мансийска выглядят ухоженными, современными и обладают всей необходимой инфраструктурой – там есть школы, детские сады, магазины, торговые комплексы, дорожные развязки, кафе.</p>\n" +
                "<br>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Ректорат</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\">\n" +
                "<div class=\"card-content\">\n" +
                "<div class=\"list-block media-list\">\n" +
                "<ul style=\"-webkit-padding-start: 0px;\">\n" +
                "<li class=\"item-content\">\n" +
                "<div class=\"item-media\">\n" +
                "<img src=\"file:///android_asset/images/b200a3e33f9f95f432a89bd926092488.jpg\" width=\"44\">\n" +
                "</div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Карминская Татьяна Дмитриевна</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Ректор</div>\n" +
                "<div class=\"item-subtitle\">(3467) 357-504</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\">\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/146f2f346eb396a98ab570d120cac3da.jpg\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Кучин Роман Викторович</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Первый проректор</div>\n" +
                "<div class=\"item-subtitle\">(3467) 357-732</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\">\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/7e233f7adbf7a43d010a1298b23f508d.jpg\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Пятков Сергей Григорьевич</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Проректор по научной работе</div>\n" +
                "<div class=\"item-subtitle\"> и международной деятельности</div>\n" +
                "<div class=\"item-subtitle\">(3467) 357-508, (3467)357-618</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\">\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/29174f719649b5c506a94a6788348b65.jpg\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Мищенко Владимир Александрович</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Проректор по учебной</div>\n" +
                "<div class=\"item-subtitle\">и воспитательной работе</div>\n" +
                "<div class=\"item-subtitle\">(3467) 357-817</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "<li class=\"item-content\">\n" +
                "<div class=\"item-media\"><img src=\"file:///android_asset/images/eb2ed2ae236ec43f8965295b45e72cbd.jpg\" width=\"44\"></div>\n" +
                "<div class=\"item-inner\">\n" +
                "<div class=\"item-title-row\">\n" +
                "<div class=\"item-title\">Дятлова Татьяна Александровна</div>\n" +
                "</div>\n" +
                "<div class=\"item-subtitle\">Проректор по социальной</div>\n" +
                "<div class=\"item-subtitle\"> и административно-хозяйственной</div>\n" +
                "<div class=\"item-subtitle\"> работе</div>\n" +
                "<div class=\"item-subtitle\">(3467) 357-748</div>\n" +
                "</div>\n" +
                "</li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Дополнительное образование</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\" style=\"padding: 0 16px;\">\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Институт дополнительного образования – ведущий центр дополнительного профессионального образования в Ханты-Мансийском автономном округе – Югре, в нем ежегодно проходят обучение более 2000 слушателей.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Информацию о программах, сроках и заявках на обучение можно получить по телефону (3467)357 610, (3467)357 654</p>\n" +
                "<br>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Международное сотрудничество</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\" style=\"padding: 0 16px;\">\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Практически во всех странах мира работают образовательные фонды, государственные программы, стипендиальные программы отдельных регионов и университетов: они предлагают стипендии, гранты и программы обмена.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Наш университет активно развивает международную деятельность, в которую активно вовлечены студенты, преподаватели и сотрудники университета. Благодаря расширению географии зарубежных партнерств, все больше студентов имеют возможность проходить стажировки за границей, а ученые – проводить совместные исследования и проекты.</p>\n" +
                "<p>&nbsp;&nbsp;&nbsp;&nbsp;Оперативная информация для студентов, аспирантов и преподавателей ЮГУ о возможностях обучения и проведения исследований за рубежом в нашей <a onclick='window.open(\"http://vk.com/ugrasu_oms\", \"_blank\")'>группе Вконтакте(http://vk.com/ugrasu_oms)</a>, по телефону 357-713 и по e-mail:  <a onclick='window.open(\"mailto:yulia.papanova@gmail.com\", \"_blank\")'>yulia.papanova@gmail.com</a>, <a onclick='window.open(\"mailto:ugrasu.international@gmail.com\", \"_blank\")'>ugrasu.international@gmail.com</a><br><br><br>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"acc-btn\">\n" +
                "<h1>Здравпункт</h1>\n" +
                "</div>\n" +
                "<div class=\"acc-content\">\n" +
                "<div class=\"acc-content-inner\" style=\"padding: 0 16px;\">\n" +
                "<p>Здравпункт находится на первом этаже 6-го корпуса<br>\n" +
                "Часы работы пн-пт с 8 до 15:30<br>\n" +
                "Обед с 12:00 до 12.30<br><br><br>\n" +
                "</p>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "<button onclick='window.open(\"http://vuzapp.ru/ugrasu/feedback\", \"_blank\");' class=\"button\" style=\"padding-bottom: 5px; margin-top: 5px; height: 36px; width: 100%;\">Улучшить справочник</button>\n" +
                "</ons-template>\n" +
                "<ons-template id=\"abiturient\">\n" +
                "<div id=\"searchprogress\" style=\"display: block;\">\n" +
                "<center>\n" +
                "<img src=\"file:///android_asset/images/load.gif\" style=\"margin-top: 96px;\">\n" +
                "</center>\n" +
                "</div>\n" +
                "<div class=\"content-block\" id=\"abiturient-data\"></div>\n" +
                "</ons-template>\n" +
                "<ons-template id=\"search\">\n" +
                "<div class=\"center\" style=\"padding: 0 16px; padding-bottom: 16px; margin-top: 44px; background-color:#2196f3;\">\n" +
                "<input type=\"text\" class=\"text-input text-input--underbar\" id=\"searchinput\" placeholder=\"Поиск\" style=\"width: 100%; height: 40px; color: #fff; border-bottom: 2px solid #fff;\">\n" +
                "<label style=\"font-size: 12px; color: #fff;\">\n" +
                "Поиск ведется по имени преподавателя, номеру телефона, отделению университета и номеру кабинета.\n" +
                "</label><br>\n" +
                "<button onclick=\"Search();\" class=\"button\" style=\"padding-bottom: 5px; margin-top: 5px; height: 36px; width: 100%;\">Искать</button>\n" +
                "</div>\n" +
                "<div id=\"searchprogress\" style=\"display: none;\">\n" +
                "<center>\n" +
                "<img src=\"file:///android_asset/images/load.gif\" style=\"margin-top: 46px;\">\n" +
                "</center>\n" +
                "</div>\n" +
                "<div id=\"SearchResults\"></div>\n" +
                "</ons-template>\n" +
                "</body>\n" +
                "<script src=\"file:///android_asset/angular.min.js\"></script>\n" +
                "<script src=\"file:///android_asset/jquery-1.11.3.min.js\"></script>\n" +
                "<script src=\"file:///android_asset/ajax.js\"></script>\n" +
                "<script src=\"file:///android_asset/onsenui.min.js\"></script>\n" +
                "<script>\n" +
                "ons.bootstrap();\n" +
                "</script>\n" +
                "<script>\n" +
                "$(document).ready(function(){\n" +
                "var animTime = 300,\n" +
                "clickPolice = false;\n" +
                "\n" +
                "$(document).on('click', '.acc-btn', function(){\n" +
                "if(!clickPolice) {\n" +
                "clickPolice = true;\n" +
                "\n" +
                "var currIndex = $(this).index('.acc-btn'),\n" +
                "targetHeight = $('.acc-content-inner').eq(currIndex).outerHeight();\n" +
                "\n" +
                "$('.acc-btn h1').removeClass('selected');\n" +
                "$(this).find('h1').addClass('selected');\n" +
                "\n" +
                "$('.acc-content').stop().animate({ height: 0 }, animTime);\n" +
                "$('.acc-content').eq(currIndex).stop().animate({ height: targetHeight }, animTime);\n" +
                "\n" +
                "setTimeout(function(){ clickPolice = false; }, animTime);\n" +
                "}\n" +
                "});\n" +
                "});\n" +
                "</script>\n" +
                "<style type=\"text/css\">\n" +
                "body {\n" +
                "top: -1px;\n" +
                "}\n" +
                "</style>" +
                "</html>";

        mWebView.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "UTF-8", null);

        return rootView;
    }
}
