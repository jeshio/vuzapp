package com.jeshio.vuzup;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 02.11.14.
 */
public class MyArrayAdapter extends ArrayAdapter {
    private int[] colors;
    private Drawable[] icons;

    public MyArrayAdapter(Context context, int layout, String[] str, Drawable[] icons, int color1, int color2) {
        super(context, layout, R.id.label, str);
        colors = new int[]{color1, color2};
        this.icons = icons;
    }

    /**
     * Display rows in alternating colors
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        view.setBackgroundColor(colors[position % colors.length]);


        return view;
    }
}
