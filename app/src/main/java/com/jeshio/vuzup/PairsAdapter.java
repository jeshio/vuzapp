package com.jeshio.vuzup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 24.12.14.
 */
public class PairsAdapter extends BaseAdapter {
    ArrayList<PairsItem> objects;
    LayoutInflater lInflater;
    private Context ctx;

    PairsAdapter(Context context, ArrayList<PairsItem> pairs) {
        ctx = context;
        objects = pairs;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }


    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.pair_item, parent, false);
        }

        PairsItem p = getPairItem(position);

        ((TextView) view.findViewById(R.id.tv_pairtime)).setText(p.begin + " " + p.end);
        ((TextView) view.findViewById(R.id.tv_pairname)).setText(p.name);
        String subGroup = p.subgroup.length() > 0 ? " (" + p.subgroup + " подгруппа)" : "";

        String pairWho;

        if (p.teacher.length() > 0)
            pairWho = p.teacher + subGroup;
        else
            pairWho = p.group + subGroup;

        ((TextView) view.findViewById(R.id.tv_pair_who)).setText(pairWho);
        ((TextView) view.findViewById(R.id.tv_auditory)).setText(p.auditory);


        return view;
    }

    PairsItem getPairItem(int position) {
        return ((PairsItem) getItem(position));
    }
}

