package com.jeshio.vuzup;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jeshio.vuzup.other.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 06.10.14.
 */
public class News extends Fragment {

    private String server = "http://api.vuzapp.ru/1.0/news.list.get?key=ugrasu&api_key=cxUKa1PhMjXALzxTYFK27BGniN5YnHSi";
    private ArrayList<NewsItem> news = new ArrayList<NewsItem>();
    private RecyclerView newsListView;
    private View rootView;
    private NewsItemAdapter newsArrayAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.news, container, false);

        newsListView = (RecyclerView) rootView.findViewById(R.id.newsListView);

        mLayoutManager = new LinearLayoutManager(getActivity());

        newsListView.setLayoutManager(mLayoutManager);


        newsArrayAdapter = new NewsItemAdapter(rootView.getContext(), news);

        newsListView.setAdapter(newsArrayAdapter);

        newsListView.addOnItemTouchListener(new RecyclerItemClickListener(inflater.getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position >= 0) {
                            int[] screenLocation = new int[2];
                            view.getLocationOnScreen(screenLocation);
                            NewsItem item = news.get(position);
                            //Intent subActivity = new Intent(getActivity().getApplicationContext(), NewsItemDetail.class);

                            NewsItemDetail nextFrag = new NewsItemDetail();

                            int orientation = getResources().getConfiguration().orientation;
                            getActivity().getIntent().putExtra("orientation", orientation).
                                    putExtra("title", item.title).
                                    putExtra("image", item.image).
                                    putExtra("date", item.date_d).
                                    putExtra("text", item.detail_text).
                                    putExtra("preview_text", item.preview_text).
                                    putExtra("left", screenLocation[0]).
                                    putExtra("top", screenLocation[1]).
                                    putExtra("width", view.getWidth()).
                                    putExtra("height", view.getHeight());

                            News.this.getFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, nextFrag, "NEWS")
                                    .addToBackStack("stack")
                                    .commit();

                            getActivity().overridePendingTransition(0, 0);
                        }
                    }
                })
        );

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        new NewsTask().execute();
    }

    class NewsTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String content = "";
            HttpURLConnection urlConnection = null;

            try {
                urlConnection = (HttpURLConnection) new URL(server).openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String temp;

                byte[] contents = new byte[1024];

                int bytesRead = 0;
                while ((bytesRead = in.read(contents)) != -1) {
                    content += new String(contents, 0, bytesRead);
                }

            } catch (IOException e) {
                return "-1";
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return content;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            JSONArray arr;

            try {
                JSONObject jObject = new JSONObject(result);
                jObject = jObject.getJSONObject("response");
                arr = jObject.getJSONArray("items");
                for (int i = 0; i < arr.length() - 1; i++) {
                    if (!arr.isNull(i)) {
                        JSONObject obj = arr.getJSONObject(i);
                        news.add(new NewsItem(obj, newsArrayAdapter));
                    }
                }
            } catch (Exception e) {
                Toast.makeText(rootView.getContext(), getResources().getString(R.string.bad_connect), Toast.LENGTH_SHORT).show();
                Log.d("msg", e.getMessage());
            }
            ;
            newsArrayAdapter.notifyDataSetChanged();
        }
    }
}