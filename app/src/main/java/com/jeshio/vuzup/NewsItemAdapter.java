package com.jeshio.vuzup;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 23.11.14.
 */
public class NewsItemAdapter extends RecyclerView.Adapter<NewsItemAdapter.ViewHolder> {
    Context ctx;
    LayoutInflater layoutInflater;
    ArrayList<NewsItem> objects;

    NewsItemAdapter(Context context, ArrayList<NewsItem> news) {
        ctx = context;
        objects = news;
        layoutInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
        return new ViewHolder(view);
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        NewsItem p = objects.get(position);
        SpannableString title = new SpannableString(p.title);
        title.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), 0);

        viewHolder.title.setText(title);
        //viewHolder.date.setText(p.date_d);
        viewHolder.text.setText(p.preview_text);
        viewHolder.icon.setImageBitmap(p.image);
/*
        try {
            img_view.getLayoutParams().width = 100;
            img_view.getLayoutParams().height = 100;
            p.image = bitmap_blur(p.image, img_view.getLayoutParams().width, img_view.getLayoutParams().height);

        } catch (Exception e) {

        }
        img_view.setAdjustViewBounds(true);
        img_view.setImageBitmap(p.image);*/
    }

    // кол-во элементов
    @Override
    public int getItemCount() {
        return objects.size();
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView date;
        private TextView text;
        private ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            title = ((TextView) itemView.findViewById(R.id.news_item_title));
            //date = ((TextView) itemView.findViewById(R.id.news_item_date));
            text = ((TextView) itemView.findViewById(R.id.news_item_short_text));
            icon = (ImageView) itemView.findViewById(R.id.news_item_image);
        }
    }


}
