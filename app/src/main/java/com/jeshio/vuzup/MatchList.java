package com.jeshio.vuzup;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.vuzapp.app.R;

/**
 * Created by Jeshio on 19.09.2015.
 */
public class MatchList extends Fragment {
    private View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.timetable, container, false);

        return rootView;
    }
}
