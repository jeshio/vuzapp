package com.jeshio.vuzup;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import ru.vuzapp.app.R;


/**
 * Created by jeshio on 06.10.14.
 */
public class Events extends Fragment {
    private WebView mWebView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.events, container, false);

        String whoIs = Perferences.getString(getActivity(), Perferences.TIMETABLE_MAIN_DATA, "");

        mWebView = (WebView) rootView.findViewById(R.id.webviewEvents);
        // включаем поддержку JavaScript
        mWebView.getSettings().setJavaScriptEnabled(true);

        String content = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "   <head>\n" +
                "      <meta charset=\"utf-8\">\n" +
                "      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui\">\n" +
                "      <meta name=\"mobile-web-app-capable\" content=\"yes\">\n" +
                "      <meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n" +
                "      <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\n" +
                "      <title>События</title>\n" +
                "      <link rel=\"stylesheet\" href=\"file:///android_asset/framework7.ios.css\">\n" +
                "      <link rel=\"stylesheet\" href=\"file:///android_asset/framework7.ios.colors.css\">\n" +
                "      <link rel=\"stylesheet\" href=\"file:///android_asset/kitchen-sink.css\">\n" +
                "   </head>\n" +
                "   <body onload=\"SendRequest('http://api.vuzapp.ru/1.0/AndroidApp/eventsList.php?key=ugrasu&api_key=cxUKa1PhMjXALzxTYFK27BGniN5YnHSi&name=" +
                whoIs +
                "');\">\n" +
                "      <div class=\"load-bar\" id=\"loader\">\n" +
                "         <div class=\"bar\"></div>\n" +
                "         <div class=\"bar\"></div>\n" +
                "         <div class=\"bar\"></div>\n" +
                "      </div>\n" +
                "      <div class=\"views\">\n" +
                "         <div class=\"view view-main\">\n" +
                "            <div class=\"pages navbar-through toolbar-through\">\n" +
                "               <div data-page=\"index\" class=\"page page-on-center\">\n" +
                "                  <div class=\"page-content\" style=\"padding-top:0px; padding-bottom: 0px;\" id=\"data\">\n" +
                "                                  </div>\n" +
                "               </div>\n" +
                "            </div>\n" +
                "         </div>\n" +
                "      </div>\n" +
                "      <script src=\"file:///android_asset/jquery-1.11.3.min.js\"></script>\n" +
                "      <script type=\"text/javascript\" src=\"file:///android_asset/framework7.min.js\"></script>\n" +
                "      <script type=\"text/javascript\" src=\"file:///android_asset/kitchen-sink.js\"></script>\n" +
                "      <script type=\"text/javascript\" src=\"file:///android_asset/ajax.js\"></script>\n" +
                "   </body>\n" +
                "</html>";

        // указываем страницу загрузки
        mWebView.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "UTF-8", null);

        return rootView;
    }
}
