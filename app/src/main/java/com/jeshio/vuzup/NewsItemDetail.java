package com.jeshio.vuzup;

import android.animation.TimeInterpolator;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.TextView;

import ru.vuzapp.app.R;


public class NewsItemDetail extends Fragment {
    private static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
    private static final TimeInterpolator sAccelerator = new DecelerateInterpolator();
    protected final long duration = 650;
    final private int ANIM_DURATION = 10;
    View rootView;
    private int orientation;
    private ImageView imageView;
    private TextView titleView,
            textView,
            dateView,
            shortTextView;
    private CardView detailBlock;
    private float leftDelta, topDelta, widthScale, heightScale;
    private float originalHeight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_news_item_detail, container, false);

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    runExitAnimation();
                    return true;
                } else {
                    return false;
                }
            }
        });


        imageView = (ImageView) rootView.findViewById(R.id.detailImage);
        titleView = (TextView) rootView.findViewById(R.id.detailTitle);
        textView = (TextView) rootView.findViewById(R.id.detailText);
        dateView = (TextView) rootView.findViewById(R.id.detailDate);
        shortTextView = (TextView) rootView.findViewById(R.id.news_item_detail_short_text);
        detailBlock = (CardView) rootView.findViewById(R.id.detailBlock);

        Bundle bundle = getActivity().getIntent().getExtras();
        String title = bundle.getString("title");
        String text = bundle.getString("text");
        String preview_text = bundle.getString("preview_text");
        String date = bundle.getString("date");
        Bitmap image = (Bitmap) bundle.get("image");
        final int thumbLeft = bundle.getInt("left");
        final int thumbTop = bundle.getInt("top");
        final int thumbWidth = bundle.getInt("width");
        final int thumbHeight = bundle.getInt("height");
        orientation = bundle.getInt("orientation");

        imageView.setImageBitmap(image);

        SpannableString titleBold = new SpannableString(title);
        titleBold.setSpan(new StyleSpan(Typeface.BOLD), 0, titleBold.length(), 0);

        titleView.setText(titleBold);
        textView.setText(text);
        dateView.setText(date);
        shortTextView.setText(preview_text);

        if (savedInstanceState == null) {
            ViewTreeObserver observer = imageView.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    imageView.getViewTreeObserver().removeOnPreDrawListener(this);

                    int[] screenLocation = new int[2];

                    detailBlock.getLocationOnScreen(screenLocation);

                    leftDelta = thumbLeft - screenLocation[0];
                    topDelta = thumbTop - screenLocation[1];

                    widthScale = (float) thumbWidth / imageView.getDrawable().getIntrinsicWidth();
                    heightScale = (float) thumbHeight / imageView.getDrawable().getIntrinsicHeight();
                    originalHeight = thumbHeight;

                    runAnimation();

                    return false;
                }
            });
        }

        return rootView;
    }

    public void runAnimation() {
        detailBlock.setTranslationY(topDelta);

        detailBlock.requestLayout();
        final int needHeight = detailBlock.getHeight();
        detailBlock.getLayoutParams().height = imageView.getHeight() + imageView.getHeight() / 15;
        detailBlock.requestLayout();
        final int startHeight = detailBlock.getLayoutParams().height;

        final Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (detailBlock.getLayoutParams().height < needHeight) {
                    detailBlock.getLayoutParams().height = startHeight + Math.round(needHeight * interpolatedTime);
                    detailBlock.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(duration);


        detailBlock.animate().setDuration(duration).translationX(0).translationY(0).setInterpolator(sDecelerator);
        detailBlock.startAnimation(a);

        imageView.animate().setDuration(duration).
                translationX(0).translationY(0).
                setInterpolator(sDecelerator);
    }

    public void runExitAnimation() {
        final int currentHeight = detailBlock.getLayoutParams().height;
        final Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (detailBlock.getLayoutParams().height > originalHeight) {
                    detailBlock.getLayoutParams().height = currentHeight - Math.round(currentHeight * interpolatedTime * 0.8f);
                    detailBlock.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(duration);

        detailBlock.startAnimation(a);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            detailBlock.animate().setDuration(duration / 3).translationY(topDelta).
                    setInterpolator(sDecelerator).withEndAction(new Runnable() {
                @Override
                public void run() {
                    getActivity().getFragmentManager().popBackStack();
                }
            });
        } else {
            getActivity().getFragmentManager().popBackStack();
        }

    }

}
