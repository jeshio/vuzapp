package com.jeshio.vuzup;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import ru.vuzapp.app.R;


public class Main extends ActionBarActivity {
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    protected ArrayAdapter<String> autocompeteAdapter;
    Fragment timetableFragment = null;
    Fragment newsFragment = null;
    Fragment eventsFragment = null;
    Fragment infoFragment = null;
    Fragment notificationsFragment = null;
    Fragment settingsFragment = null;
    Integer currentFragment;
    ArrayList<FragmentWithTag> fragmentStack = new ArrayList<>();
    private FrameLayout titleInputLayout;
    private FrameLayout titleLayout;
    private ActionBarDrawerToggle myDrawerToggle;
    private TextView tvPageTitle;
    private ImageButton searchButton;
    private AutoCompleteTextView searchQuery;
    private String[] viewsNames;
    private ArrayList<String> listAutoComplete = new ArrayList<String>();
    private Drawer drawer;
    private boolean drawerSelect = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(60);

        tracker = analytics.newTracker("UA-71014600-1");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);

        // Handle Toolbar
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Perferences.getString(this, Perferences.TIMETABLE_MAIN_DATA, "").length() != 0 &&
                Perferences.getInt(this, Perferences.UPDATE_MSG, 0) == 0) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage(R.string.update_msg);
            dlgAlert.setTitle(R.string.update_title);
            dlgAlert.setPositiveButton(R.string.update_ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
            Perferences.setInt(this, Perferences.UPDATE_MSG, 1);
        }

        String mainData = Perferences.getString(this, Perferences.TIMETABLE_MAIN_DATA, "");

        if (mainData.length() > 0 && mainData.matches("([а-яА-я .]+)")) {
            Perferences.setInt(this, Perferences.TIMETABLE_TYPE, Perferences.TYPE_PREPODS);
        }

        // load slide menu items
        viewsNames = getResources().getStringArray(R.array.views_array);
        tvPageTitle = (TextView) findViewById(R.id.pageTitle);
        titleLayout = (FrameLayout) findViewById(R.id.titleLayout);
        titleInputLayout = (FrameLayout) findViewById(R.id.titleInputLayout);
        searchButton = (ImageButton) findViewById(R.id.searchButton);
        searchQuery = (AutoCompleteTextView) findViewById(R.id.searchQuery);

        autocompeteAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listAutoComplete);

        autocompeteAdapter.setNotifyOnChange(true);

        searchQuery.setAdapter(autocompeteAdapter);

        searchQuery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);
                Bundle b = new Bundle();
                b.putString(MyApp.oneShot, selected);
                displayView(11, b, "(" + selected + ")");
                showTitleLayout();
            }
        });

        new AutocompleteList().execute(MyApp.server_prepods);
        new AutocompleteList().execute(MyApp.server_students);

        String whoIs = Perferences.getString(this, Perferences.TIMETABLE_MAIN_DATA, "");

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            if (whoIs.length() == 0)
                displayView(6);
            else
                displayView(0);
        }

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withTranslucentStatusBar(true)
                .withActionBarDrawerToggle(true)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(viewsNames[0]).withTypeface(Typeface.DEFAULT_BOLD).withIcon(GoogleMaterial.Icon.gmd_reader),
                        new PrimaryDrawerItem().withName(viewsNames[1]).withTypeface(Typeface.DEFAULT_BOLD).withIcon(GoogleMaterial.Icon.gmd_time_countdown),
                        new PrimaryDrawerItem().withName(viewsNames[2]).withTypeface(Typeface.DEFAULT_BOLD).withIcon(GoogleMaterial.Icon.gmd_calendar_note),
                        new PrimaryDrawerItem().withName(viewsNames[3]).withTypeface(Typeface.DEFAULT_BOLD).withIcon(GoogleMaterial.Icon.gmd_notifications),
                        new PrimaryDrawerItem().withName(viewsNames[4]).withTypeface(Typeface.DEFAULT_BOLD).withIcon(GoogleMaterial.Icon.gmd_info),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(viewsNames[5]).withTypeface(Typeface.DEFAULT_BOLD).withIcon(GoogleMaterial.Icon.gmd_settings),
                        new SecondaryDrawerItem().withName(viewsNames[6]).withTypeface(Typeface.DEFAULT_BOLD).withIcon(GoogleMaterial.Icon.gmd_help)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerSelect)
                            displayView(position - 1);
                        drawerSelect = true;
                        return false;
                    }
                })
                .build();


        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showSearchInputLayout();
            }
        });

        searchQuery.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    showTitleLayout();
                }
            }
        });

        drawer.openDrawer();
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (searchQuery.hasFocus())
            showTitleLayout();
        else {
            if (fragmentStack.size() > 1) {
                fragmentStack.remove(fragmentStack.size() - 1);
                FragmentWithTag f = fragmentStack.get(fragmentStack.size() - 1);

                android.app.FragmentManager fragmentManager = getFragmentManager();
                android.app.FragmentTransaction trans = fragmentManager.beginTransaction();
                trans.replace(R.id.content_frame, f.fragment, f.tag);
                trans.addToBackStack("stack");
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                trans.commit();
                int index = Integer.parseInt(f.tag);
                setTitleView(index);

                drawerSelect = false;
                drawer.setSelectionAtPosition(index + 1);
            } else {
                super.onBackPressed();
            }
        }
    }

    private void showSearchInputLayout() {
        titleLayout.setVisibility(View.INVISIBLE);
        titleInputLayout.setVisibility(View.VISIBLE);
        // передача фокуса и вывод клавиатуры
        searchQuery.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchQuery, InputMethodManager.SHOW_IMPLICIT);
    }

    private void showTitleLayout() {
        titleLayout.setVisibility(View.VISIBLE);
        titleInputLayout.setVisibility(View.INVISIBLE);
    }

    private void setPageTitle(String title) {
        tvPageTitle.setText(title);
    }

    private void displayView(int position) {
        setView(position, new Bundle(), "");
    }

    private void displayView(int position, Bundle args) {
        setView(position, args, "");
    }

    private void displayView(int position, Bundle args, String postTitle) {
        setView(position, args, postTitle);
    }

    private void setView(int position, Bundle args, String postTitle) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                if (newsFragment == null) {
                    newsFragment = new News();
                }
                fragment = newsFragment;
                break;
            case 1:
                if (timetableFragment == null) {
                    timetableFragment = new Timetable();
                }
                fragment = timetableFragment;
                break;
            case 2:
                if (eventsFragment == null) {
                    eventsFragment = new Events();
                }
                fragment = eventsFragment;
                break;
            case 3:
                if (notificationsFragment == null) {
                    notificationsFragment = new Notifications();
                }
                fragment = notificationsFragment;
                break;
            case 4:
                if (infoFragment == null) {
                    infoFragment = new Info();
                }
                fragment = infoFragment;
                break;
            case 6:
                if (settingsFragment == null) {
                    settingsFragment = new Settings();
                }
                fragment = settingsFragment;
                break;
            case 7:
                Uri uriUrl = Uri.parse("http://vuzapp.ru/ugrasu/feedback");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
                break;
            case 11:
                fragment = new Timetable();
                break;
            default:
                break;
        }

        setTitleView(position, postTitle);

        String tag = "" + position;

        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentWithTag f = new FragmentWithTag(fragment, tag);

        if (fragment != null) {

            android.app.FragmentTransaction trans = fragmentManager.beginTransaction();
            Fragment frg = fragmentManager.findFragmentByTag(tag);

            if (!args.isEmpty()) {
                fragment.setArguments(args);
                if (frg != null) {
                    trans.remove(frg);
                    trans.commit();
                    fragmentManager.popBackStack();
                    trans = fragmentManager.beginTransaction();
                }
            }

            trans.replace(R.id.content_frame, fragment, tag);

            trans.addToBackStack("stack");

            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            trans.commit();

            if (fragmentStack.indexOf(f) != -1)
                fragmentStack.remove(fragmentStack.indexOf(f));

            fragmentStack.add(f);

            currentFragment = position;
        }
    }

    public void setTitleView(int position) {
        setTitleView(position, "");
    }

    public void setTitleView(int position, String postTitle) {
        if (position > 4)
            position--;
        if (postTitle.length() > 0)
            postTitle = " " + postTitle;

        if (position != 6) {
            if (position != 10)
                setPageTitle(viewsNames[position] + postTitle);
            else
                setPageTitle(viewsNames[1] + postTitle);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        //myDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        myDrawerToggle.onConfigurationChanged(newConfig);
    }

    class AutocompleteList extends TimetableMainDataTask {
        @Override
        protected void postExecuteFinish() {
            super.postExecuteFinish();

            for (int i = 0; i < this.list.size(); i++) {
                listAutoComplete.add(this.list.get(i));
            }

            autocompeteAdapter.notifyDataSetChanged();
        }

        @Override
        protected String doInBackground(String... params) {
            String content = "";
            HttpURLConnection urlConnection = null;

            try {
                urlConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String temp;

                byte[] contents = new byte[1024];

                int bytesRead = 0;
                while ((bytesRead = in.read(contents)) != -1) {
                    content += new String(contents, 0, bytesRead);
                }

            } catch (IOException e) {
                return "-1";
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return content;
        }
    }

    class FragmentWithTag {
        Fragment fragment;
        String tag;

        public FragmentWithTag(Fragment f, String t) {
            fragment = f;
            tag = t;
        }

        @Override
        public boolean equals(Object o) {
            FragmentWithTag f = (FragmentWithTag) o;
            return tag.equals(f.tag);
        }

        @Override
        public String toString() {
            return tag;
        }
    }
}
