package com.jeshio.vuzup;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jeshio on 23.11.14.
 */
public class Perferences {
    static final String TIMETABLE_TYPE = "timetable_type";
    static final String TIMETABLE_MAIN_DATA = "timetable_main_data";
    static final String LAST_UPDATE = "last_update";
    static final int TYPE_STUDENTS = 1;
    static final int TYPE_PREPODS = 2;
    static final String UPDATE_MSG = "update_msg";
    static private final String APP_PERFERENCES = "timetable_app";

    static void setString(Context c, String param, String value) {
        Perferences.getEditor(c).putString(param, value).apply();
    }

    static void setBool(Context c, String param, Boolean value) {
        Perferences.getEditor(c).putBoolean(param, value).apply();
    }

    static void setInt(Context c, String param, int value) {
        Perferences.getEditor(c).putInt(param, value).apply();
    }

    static String getString(Context c, String param, String def) {
        return Perferences.getSharedPerferences(c).getString(param, def);
    }

    static Boolean getBool(Context c, String param, Boolean def) {
        return Perferences.getSharedPerferences(c).getBoolean(param, def);
    }

    static int getInt(Context c, String param, int def) {
        return Perferences.getSharedPerferences(c).getInt(param, def);
    }

    static private SharedPreferences.Editor getEditor(Context c) {
        return Perferences.getSharedPerferences(c).edit();
    }

    static private SharedPreferences getSharedPerferences(Context c) {
        return c.getSharedPreferences(Perferences.APP_PERFERENCES, Context.MODE_PRIVATE);
    }

    // проверить, используется ли основное расписание для студента
    static boolean studentApi(Context rootView) {
        return Perferences.getInt(rootView,
                Perferences.TIMETABLE_TYPE, Perferences.TYPE_STUDENTS) == Perferences.TYPE_STUDENTS;
    }
}
