package com.jeshio.vuzup;

import java.util.ArrayList;

/**
 * Created by jeshio on 20.12.14.
 */
public class WeekDayItem {
    public String weekDay;
    public ArrayList<PairsItem> pairs;

    WeekDayItem(String date, ArrayList<PairsItem> pairs) {
        this.pairs = pairs;
        weekDay = date;
    }

    public void addPairs(ArrayList<PairsItem> pairs) {
        for (PairsItem pair :
                pairs) {
            this.pairs.add(pair);
        }
    }

    @Override
    public String toString() {
        return "weekDay: " + weekDay + ", pairs: " + pairs.toString();
    }
}
