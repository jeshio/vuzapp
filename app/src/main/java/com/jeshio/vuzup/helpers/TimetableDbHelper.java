package com.jeshio.vuzup.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Jeshio on 25.08.2015.
 */
public class TimetableDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Timetable.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TimetableEntry.TABLE_NAME + " (" +
                    TimetableEntry._ID + " INTEGER PRIMARY KEY," +
                    TimetableEntry.COLUMN_NAME_INSTITUTE + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_DATE + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_BEGIN + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_END + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_GROUP + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_SUBGROUP + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_TEACHER + TEXT_TYPE + COMMA_SEP +
                    TimetableEntry.COLUMN_NAME_AUDITORY + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TimetableEntry.TABLE_NAME;


    public TimetableDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public static abstract class TimetableEntry implements BaseColumns {
        public static final String TABLE_NAME = "timetable";
        public static final String COLUMN_NAME_INSTITUTE = "institute";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_BEGIN = "begin";
        public static final String COLUMN_NAME_END = "end";
        public static final String COLUMN_NAME_GROUP = "'group'";
        public static final String COLUMN_NAME_SUBGROUP = "subgroup";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_TEACHER = "teacher";
        public static final String COLUMN_NAME_AUDITORY = "auditory";
    }


}
