package com.jeshio.vuzup;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 22.12.14.
 */
public class Notifications extends Fragment {
    private WebView mWebView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.notifications, container, false);

        String whoIs = Perferences.getString(getActivity(), Perferences.TIMETABLE_MAIN_DATA, "");

        mWebView = (WebView) rootView.findViewById(R.id.webviewNotification);
        // включаем поддержку JavaScript
        mWebView.getSettings().setJavaScriptEnabled(true);
        // указываем страницу загрузки
        String content = "<!DOCTYPE html><html><head>" +
                "<meta charset=utf-8>" +
                "<meta name=viewport content='width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no,minimal-ui'>" +
                "<meta name=mobile-web-app-capable content=yes>" +
                "<meta name=apple-mobile-web-app-capable content=yes>" +
                "<meta name=apple-mobile-web-app-status-bar-style content=black>" +
                "<title>Уведомления</title>" +
                "<link rel=stylesheet href='file:///android_asset/framework7.ios.css'>" +
                "<link rel=stylesheet href='file:///android_asset/framework7.ios.colors.css'>" +
                "<link rel=stylesheet href='file:///android_asset/kitchen-sink.css'>" +
                "</head><body onload=\"SendRequest('http://api.vuzapp.ru/1.0/AndroidApp/notifyList.php?key=ugrasu&api_key=cxUKa1PhMjXALzxTYFK27BGniN5YnHSi&version=1.0.1&name="
                + whoIs +
                "')\">" +
                "<div class=\"load-bar\" id=\"loader\">" +
                "<div class=bar></div>" +
                "<div class=bar></div>" +
                "<div class=bar></div>" +
                "</div>" +
                "<div class=views><div class=\"view view-main\"><div class=\"pages navbar-through toolbar-through\">" +
                "<div data-page=index class=\"page page-on-center\">" +
                "<div class=page-content style=\"padding-top:0px; padding-bottom: 0px\" id=data>" +
                "</div></div></div></div></div>" +
                "<script src='file:///android_asset/jquery-1.11.3.min.js'></script>" +
                "<script type=text/javascript src='file:///android_asset/framework7.js'></script>" +
                "<script type=text/javascript src='file:///android_asset/kitchen-sink.js'></script>" +
                "<script type=text/javascript src='file:///android_asset/ajax.js'></script>" +
                "</body></html>";

        mWebView.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "UTF-8", null);

        return rootView;
    }
}