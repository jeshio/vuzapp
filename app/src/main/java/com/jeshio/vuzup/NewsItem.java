package com.jeshio.vuzup;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.LruCache;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by jeshio on 23.11.14.
 */
public class NewsItem {
    public String date_d;
    public String title;
    public String detail_link;
    public Bitmap image;
    public String preview_text;
    public String detail_text;
    private NewsItemAdapter arrayAdapter;
    private LruCache<String, Bitmap> mMemoryCache;

    NewsItem(JSONObject obj, NewsItemAdapter arrayAdapter) {
        // КЕШИРОВАНИЕ
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };

        this.arrayAdapter = arrayAdapter;
        try {
            date_d = obj.getString("date_d");
            title = obj.getString("title").replaceAll("&quot;", " ");
            detail_link = obj.getString("detail_link");
            //new DownloadImageTask().execute(obj.getString("image"));
            loadBitmap(obj.getString("image"));
            preview_text = obj.getString("preview_text");
            detail_text = obj.getString("detail_text");
        } catch (Exception e) {

        }
    }

    static Bitmap downloadBitmap(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (key != null && bitmap != null && getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public void loadBitmap(String url) {
        final String imageKey = url;

        final Bitmap bitmap = getBitmapFromMemCache(imageKey);
        if (bitmap != null) {
            image = bitmap;
        } else {
            new DownloadImageTask().execute(url);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {


        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];

            final Bitmap bitmap = downloadBitmap(urldisplay);
            addBitmapToMemoryCache(urldisplay, bitmap);

            return bitmap;
        }

        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            if (bitmap != null)
                image = bitmap;
            arrayAdapter.notifyDataSetChanged();
        }
    }
}
