package com.jeshio.vuzup;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jeshio on 07.10.2015.
 */
class TimetableTask extends AsyncTask<String, String, String> {
    protected String find_query = "";
    ArrayList<WeekDayItem> week_days;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String content = "";
        HttpURLConnection urlConnection = null;

        try {
            urlConnection = (HttpURLConnection) new URL(params[0] + find_query).openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            String temp;

            byte[] contents = new byte[1024];

            int bytesRead = 0;
            while ((bytesRead = in.read(contents)) != -1) {
                content += new String(contents, 0, bytesRead);
            }

        } catch (IOException e) {
            return "-1";
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return content;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        week_days = new ArrayList<WeekDayItem>();

        JSONArray arr;

        try {

            JSONObject jObject = new JSONObject(result);

            jObject = jObject.getJSONObject("response");

            arr = jObject.getJSONArray("items");

            for (int i = 0; i < arr.length(); i++) {
                ArrayList<PairsItem> pairs = new ArrayList<PairsItem>();

                JSONObject JWeekDays = arr.getJSONObject(i);

                // *

                JSONArray JWeekDay = JWeekDays.getJSONArray("items");

                for (int j = 0; j < JWeekDay.length(); j++) {
                    pairs.add(new PairsItem(JWeekDay.getJSONObject(j), JWeekDays.getString("date_d").trim()));
                }

                week_days.add(new WeekDayItem(JWeekDays.getString("date_d").trim(), pairs));
            }

        } catch (Exception e) {
            Log.d("Response Exception", e.getMessage());
        }
    }
}
