package com.jeshio.vuzup;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 06.10.14.
 */
public class Settings extends Fragment {
    TextView tv_main_data;
    EditText et_main_data;
    ListView lv_main_data;
    ArrayAdapter<String> autocompleteArrayAdapter;
    ArrayList<String> autocompleteList = new ArrayList<String>();
    ArrayList<String> acVars = new ArrayList<String>();
    View rootView;

    private String current_server;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.settings, container, false);

        RadioGroup rg = (RadioGroup) rootView.findViewById(R.id.timetableTypeGroup);

        tv_main_data = (TextView) rootView.findViewById(R.id.timetableMainDataText);

        et_main_data = (EditText) rootView.findViewById(R.id.timetableMainData);

        lv_main_data = (ListView) rootView.findViewById(R.id.timetableMainDataList);

        autocompleteArrayAdapter = new ArrayAdapter<String>(rootView.getContext(), android.R.layout.simple_list_item_1, autocompleteList);

        lv_main_data.setAdapter(autocompleteArrayAdapter);
        lv_main_data.setTextFilterEnabled(true);
        lv_main_data.setVisibility(View.INVISIBLE);

        lv_main_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                et_main_data.setText(parent.getItemAtPosition(position).toString());
                lv_main_data.setVisibility(View.INVISIBLE);
            }
        });

        if (Perferences.studentApi(rootView.getContext())) {
            current_server = MyApp.server_students;
            tv_main_data.setText(getResources().getString(R.string.timetable_group));
            ((RadioButton) rootView.findViewById(R.id.radio_type_student)).toggle();
        } else {
            current_server = MyApp.server_prepods;
            tv_main_data.setText(getResources().getString(R.string.timetable_prepod));
            ((RadioButton) rootView.findViewById(R.id.radio_type_prepod)).toggle();
        }

        et_main_data.setText(Perferences.getString(rootView.getContext(), Perferences.TIMETABLE_MAIN_DATA, ""));

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_type_prepod:
                        tv_main_data.setText(getResources().getString(R.string.timetable_prepod));
                        current_server = MyApp.server_prepods;
                        break;
                    case R.id.radio_type_student:
                        tv_main_data.setText(getResources().getString(R.string.timetable_group));
                        current_server = MyApp.server_students;
                        break;
                }
                setType();
                new TimetableList().execute(current_server);
            }
        });

        et_main_data.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateAClist(s.toString());
                setMainData(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                setMainData(s.toString());
            }
        });

        new TimetableList().execute(current_server);

        return rootView;
    }

    private void updateAClist(String s) {
        autocompleteList.clear();
        if (s.length() > 0 &&
                !Perferences.getString(rootView.getContext(), Perferences.TIMETABLE_MAIN_DATA, "").equals(s)) {
            for (String obj : acVars)
                if (obj.toLowerCase().startsWith(s.toLowerCase()))
                    autocompleteList.add(obj);
            lv_main_data.setVisibility(View.VISIBLE);
        } else {
            lv_main_data.setVisibility(View.INVISIBLE);
        }
        autocompleteArrayAdapter.notifyDataSetChanged();
    }

    private void setMainData(String s) {
        if (acVars.contains(s) &&
                !Perferences.getString(rootView.getContext(), Perferences.TIMETABLE_MAIN_DATA, "").equals(s)) {
            Perferences.setString(rootView.getContext(), Perferences.TIMETABLE_MAIN_DATA, s);
            notice_saved();
        }
    }

    private void notice_saved() {
        Toast.makeText(rootView.getContext(), getResources().getString(R.string.saved), Toast.LENGTH_SHORT).show();
    }

    private void setType() {
        int type = -1;
        if (current_server.equals(MyApp.server_prepods)) {
            if (Perferences.getInt(rootView.getContext(),
                    Perferences.TIMETABLE_TYPE, 0) != Perferences.TYPE_PREPODS) {
                type = Perferences.TYPE_PREPODS;
            }
        } else {
            if (Perferences.getInt(rootView.getContext(),
                    Perferences.TIMETABLE_TYPE, 0) != Perferences.TYPE_STUDENTS) {
                type = Perferences.TYPE_STUDENTS;
            }
        }
        if (type > -1) {
            Perferences.setString(rootView.getContext(), Perferences.TIMETABLE_MAIN_DATA, "");
            Perferences.setInt(rootView.getContext(), Perferences.TIMETABLE_TYPE, type);
        }
    }

    class TimetableList extends TimetableMainDataTask {
        @Override
        protected void postExecuteFinish() {
            super.postExecuteFinish();
            acVars = this.list;

            updateAClist(et_main_data.getText().toString());
            setMainData(et_main_data.getText().toString());
        }
    }


}
