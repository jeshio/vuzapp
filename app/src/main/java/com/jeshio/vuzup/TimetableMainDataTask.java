package com.jeshio.vuzup;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jeshio on 24.09.2015.
 */
public class TimetableMainDataTask extends AsyncTask<String, Void, String> {

    protected ArrayList<String> list = new ArrayList<String>();

    protected void postExecuteStart() {
        list.clear();
    }

    protected void postExecuteFinish() {

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String current_server = params[0];

        String content = "";
        HttpURLConnection urlConnection = null;

        try {
            urlConnection = (HttpURLConnection) new URL(current_server).openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            String temp;

            byte[] contents = new byte[1024];

            int bytesRead = 0;
            while ((bytesRead = in.read(contents)) != -1) {
                content += new String(contents, 0, bytesRead);
            }

        } catch (IOException e) {
            return "-1";
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return content;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        this.postExecuteStart();

        JSONArray arr;
        try {
            JSONObject jObject = new JSONObject(result);
            jObject = jObject.getJSONObject("response");
            arr = jObject.getJSONArray("elements");
            for (int i = 0; i < arr.length() - 1; i++) {
                JSONObject obj = arr.getJSONObject(i);
                list.add(obj.getString("name"));
            }
        } catch (Exception e) {
            //list.add("Не удалось распознать данные.");
        }

        this.postExecuteFinish();

    }
}