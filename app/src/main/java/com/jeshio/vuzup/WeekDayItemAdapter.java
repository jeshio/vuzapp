package com.jeshio.vuzup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import ru.vuzapp.app.R;

/**
 * Created by jeshio on 20.12.14.
 */
public class WeekDayItemAdapter extends BaseAdapter {
    ArrayList<WeekDayItem> objects;
    LayoutInflater lInflater;
    String date_format = "yyyy-MM-dd";
    private Context ctx;

    WeekDayItemAdapter(Context context, ArrayList<WeekDayItem> weekDays) {
        ctx = context;
        objects = weekDays;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void notifyDataSetChanged() {

        Comparator<WeekDayItem> myComparator = new Comparator<WeekDayItem>() {
            public int compare(WeekDayItem obj1, WeekDayItem obj2) {
                try {
                    SimpleDateFormat dateParser = new SimpleDateFormat(date_format);

                    return dateParser.parse(obj1.weekDay).after(dateParser.parse(obj2.weekDay)) ? 1 : -1;
                } catch (Exception e) {
                }

                return 0;
            }
        };

        Collections.sort(objects, myComparator);

        super.notifyDataSetChanged();
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }


    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.weekday_item, parent, false);
        }

        WeekDayItem p = getWeekDayItem(position);
        PairsAdapter pairsAdapter = new PairsAdapter(ctx, p.pairs);

        ListView list = ((ListView) view.findViewById(R.id.pairs_list));

        SimpleDateFormat dateParser = new SimpleDateFormat(date_format);
        String date;

        Calendar current = Calendar.getInstance();
        current.set(current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DATE), 0, 0, 0);
        current.clear(Calendar.MILLISECOND);

        try {
            java.util.Date realDate = dateParser.parse(p.weekDay);
            if (realDate.getTime() == current.getTime().getTime()) {
                date = "Сегодня (" + (new SimpleDateFormat("EEEE").format(realDate)) + ")";
            } else if (realDate.getTime() - 24 * 60 * 60 * 1000 ==
                    current.getTime().getTime()) {
                date = "Завтра (" + (new SimpleDateFormat("EEEE").format(realDate)) + ")";
            } else {
                date = new SimpleDateFormat("EEEE - dd MMMM").format(realDate);
            }
        } catch (Exception e) {
            date = p.weekDay;
        }

        ((TextView) view.findViewById(R.id.tv_weekday)).setText(date);
        list.setAdapter(pairsAdapter);
        int pairNameWidth = 0;

        int totalHeight = 0;
        for (int i = 0; i < pairsAdapter.getCount(); i++) {
            View listItem = pairsAdapter.getView(i, null, list);

            TextView TvPairName = ((TextView) listItem.findViewById(R.id.tv_pairname));

            pairNameWidth = TvPairName.getLayoutParams().width;

            int widthSpec = View.MeasureSpec.makeMeasureSpec(pairNameWidth, View.MeasureSpec.EXACTLY);
            listItem.measure(widthSpec, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams _params = list.getLayoutParams();

        _params.height = totalHeight + (list.getDividerHeight() * (p.pairs.size() - 1));
        list.setLayoutParams(_params);
        list.requestLayout();


        return view;
    }

    WeekDayItem getWeekDayItem(int position) {
        return ((WeekDayItem) getItem(position));
    }
}
